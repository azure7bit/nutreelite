<?php

  class SiteController extends Controller {
    public function actions() {
      return array(
        'captcha' => array(
          'class' => 'CCaptchaAction',
          'backColor' => 0xFFFFFF,
        ),
        'page' => array(
          'class' => 'CViewAction',
        ),
      );
    }

    protected function afterRender($view, &$output) {
      parent::afterRender($view, $output);
      Yii::app()->facebook->initJs($output);
      Yii::app()->facebook->renderOGMetaTags();
      return true;
    }

    /*
     * Login facebook PHP code
     */
    public function actionFblogin() {
      try {
        $params = array(
          'scope' => 'user_likes,read_stream,publish_stream,publish_actions,manage_pages,email,user_checkins',
          'redirect_uri' => Yii::app()->request->hostInfo . Yii::app()->request->baseURL . '/site/tutorial'
        );
        $url = Yii::app()->facebook->getLoginURL($params);
        $this->redirect($url);
      } catch (FacebookApiException $e) {
        echo $e;
      }
    }

    /*
     * Logout facebook PHP code
     */
    public function actionFblogout() {
      Yii::app()->facebook->destroySession();
      $this->redirect(array('site/index'));
    }

    /*
     * detect like facebook page PHP
    */
    private function fbLikes() {
      $user = Yii::app()->facebook->getUser();
      if ($user) {
        try {
          $fan_id = '652517128126836';
          $datas = Yii::app()->facebook->api("me/likes/$fan_id", 'GET');
          $like = $datas['data'];
          $count = count($like);
          return $count;
        } catch (FacebookApiException $e) {
          $this->redirect(array('site/index'));
        }
      } else {
        $e = "Koneksi ke Facebook Terputus";
        return $e;
      }
    }

    /*
     * Share facebook PHP code
    */
    public function actionFbshare() {
      $user = Yii::app()->facebook->getUser();
      try {
        $publishStream = Yii::app()->facebook->api("/$user/feed", 'post', array(
          'message' => 'Check out 25 labs',
          'link' => 'http://25labs.com',
          'picture' => 'http://25labs.com/images/25-labs-160-160.jpg',
          'name' => '25 labs',
          'caption' => '25labs.com',
          'description' => 'A Technology Laboratory. Highly Recomented technology blog.',
        ));
      } catch (FacebookApiException $e) {
        error_log($e);
      }
    }

    public function actionAboutus() {
      $criteria = new CDbCriteria();
      $criteria->order = 'created_at desc';
      $count = Article::model()->count($criteria);
      $pages = new CPagination($count);

      // results per page
      $pages->pageSize=3;
      $pages->applyLimit($criteria);
      $data = Article::model()->findAll($criteria);

      $userdata = User::model()->findBySql('SELECT * FROM user WHERE fb_id = "' . Yii::app()->facebook->getUser() . '"');
      if (count($userdata) != 0) {
        $return = ReturnLink::model()->findBySql('SELECT * FROM return_link WHERE fb_id = "' . $userdata->fb_id . '"');
        if(isset($_POST['User'])){
          $userdata->point = $userdata->point + 20;
          if($userdata->tree_cond >= 2 && $userdata->tree_cond <= 5){ $userdata->tree_cond = $userdata->tree_cond + 1; }
          $userdata->save();
        }
        $this->render('about', array('data' => $data, 'return'=>$return, 'userdata'=>$userdata,'pages' => $pages));
      } else {
        $this->redirect(array('site/index'));
      }
    }

    public function actionRules() {
      $this->render('rules');
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
    */
    public function actionIndex() {
      // renders the view file 'protected/views/site/index.php'
      // using the default layout 'protected/views/layouts/main.php'
      if (isset($_GET['d'])) {
        Yii::app()->session['sharer'] = $_GET['d'];
      }
      if (Yii::app()->facebook->getUser() != 0) {
        $this->redirect(array('site/register', ));
      } else {
        $this->layout = false;
        $this->render('index');
      }
    }

    public function actionRegister() {
      if (Yii::app()->facebook->getUser() != 0) {
        $data = User::model()->findBySql('SELECT * FROM user WHERE fb_id = "' . Yii::app()->facebook->getUser() . '"');
        if (count($data) == 0) {
          $model = new User;

          if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            $model->fb_id = Yii::app()->facebook->getUser();
            $model->point = 10;
            $model->tree_cond = 5;
            $model->created_time = date('Y-m-d H:i:s');
            if ($model->save()) {
              if (isset(Yii::app()->session['sharer'])) {
                $share = new ReturnLink;
                $_POST['ReturnLink'] = true;
                if (isset($_POST['ReturnLink'])) {
                  $share->attributes = $_POST['ReturnLink'];
                  $share->fb_id = Yii::app()->session['sharer'];
                  $share->clicker = Yii::app()->facebook->getUser();
                  $share->created_time = date('Y-m-d H:i:s');

                  if ($share->save()) {
                    $userShare = User::model()->findBySql('SELECT * FROM user WHERE fb_id = "' . Yii::app()->session['sharer'] . '"');
                    $userShare->point = $userShare->point + 50;
                    if($userShare->tree_cond >= 2 && $userShare->tree_cond <= 5){
                      $userShare->tree_cond = $userShare->tree_cond + 1;
                    }
                    $userShare->save();
                  }
                }
              }
              $this->redirect(array('site/tutorial'));
            }
          }
          $this->render('register', array('model' => $model, 'data'=>$data));
        } else {
          $this->redirect(array('site/tutorial'));
        }
      } else {
        $this->redirect(array('site/index'));
      }
    }

    public function actionHome() {
      $data = User::model()->findBySql('SELECT * FROM user WHERE fb_id = "' . Yii::app()->facebook->getUser() . '"');
      if (count($data) != 0) {
        $datex= date_create($data->created_time);
        $dd = date_add($datex, date_interval_create_from_date_string('3 days'));
        $yy = date_format($dd, 'Y-m-d Y-m-d H:i:s');
        $xx = date("Y-m-d H:i:s");

        if($xx > $yy){
          if($data->tree_cond >=2 && $data->tree_cond <= 5){
            $data->tree_cond = $data->tree_cond - 1;
            $data->created_time = date("Y-m-d H:i:s");
            $data->save();
          }
        }
        $this->render('home', array('data' => $data));
      } else {
        $this->redirect(array('site/index'));
      }
    }

    public function actionLeaderboard() {
      $data = User::model()->findBySql('SELECT * FROM user WHERE fb_id = "' . Yii::app()->facebook->getUser() . '"');
      if (count($data) != 0) {
        $criteria = new CDbCriteria();
        $criteria->order = 'point desc';
        $count = User::model()->count($criteria);
        $pages = new CPagination($count);

        // results per page
        $pages->pageSize = 20;
        $pages->applyLimit($criteria);

        $usersData = User::model()->findAll($criteria);;
        $this->render('leaderboard', array('data' => $data, 'usersData' => $usersData, 'pages' => $pages));
      } else {
        $this->redirect(array('site/index'));
      }
    }

    public function actionGetpoint() {
      $data = User::model()->findBySql('SELECT * FROM user WHERE fb_id = "' . Yii::app()->facebook->getUser() . '"');
      if (count($data) != 0) {
        $return = ReturnLink::model()->findBySql('SELECT * FROM return_link WHERE fb_id = "' . $data->fb_id . '"');

        $question = Question::model()->findAllBySql('SELECT * FROM question WHERE schedule < "' . date('Y-m-d H:i:s') . '"');
        $dataAns = Answer::model()->findAllBySql('SELECT * FROM answer WHERE fb_id = "' . $data->fb_id . '"');

        $model = new Answer;

        if (isset($_POST['Answer'])) {
          $model->attributes = $_POST['Answer'];
          $model->fb_id = $data->fb_id;
          $model->question_id = $question[count($dataAns)]->id;
          $model->created_time = date('Y-m-d H:i:s');
          $userUpdate = User::model()->findBySql('SELECT * FROM user WHERE fb_id = "' . $data->fb_id . '"');
          if ($question[count($dataAns)]->answer == $model->answer) {
            $userUpdate->point = $userUpdate->point + 20;
            if($userUpdate->tree_cond >= 3 && $userUpdate->tree_cond <= 5){
              $userUpdate->tree_cond = $userUpdate->tree_cond + 1;
            }
            $userUpdate->save();
          }
          if ($model->save()) {
            $this->refresh();
          }
        }
          $this->render('getpoint', array('data' => $data, 'return' => $return, 'question' => $question, 'dataAns' => $dataAns, 'model' => $model));
      } else {
        $this->redirect(array('site/index'));
      }
    }

    public function actionShare() {
      $data = User::model()->findBySql('SELECT * FROM user WHERE fb_id = "' . Yii::app()->facebook->getUser() . '"');
      if (count($data) != 0) {
        if (isset($_POST['p'])) {
          $data_share = DataShare::model()->findBySql('SELECT * FROM data_share WHERE fb_id = "' . Yii::app()->facebook->getUser() . '" AND created_time LIKE "' . date('Y-m-d') . '%" AND via = "' . $_POST['p'] . '"');
          if (count($data_share) == 0) {
            $model = new DataShare;

            $_POST['DataShare'] = true;
            if (isset($_POST['DataShare'])) {
              $model->attributes = $_POST['DataShare'];
              $model->fb_id = Yii::app()->facebook->getUser();
              $model->via = $_POST['p'];
              $model->created_time = date('Y-m-d H:i:s');
              if ($model->save()) {
                $data->point = $data->point + 50;
                if($data->tree_cond >= 3 && $data->tree_cond <= 5){
                  $data->tree_cond = $data->tree_cond + 1;
                }
                $data->save();
              } else {
                echo 'error';
              }
            } else {
              echo 'error';
            }
          } else {
            echo 'error';
          }
        }
      } else {
        echo 'error';
      }
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
      if ($error = Yii::app()->errorHandler->error) {
        if (Yii::app()->request->isAjaxRequest)
          echo $error['message'];
      else
        $this->render('home', $error);
      }
    }

    /**
     * Displays the contact page
     */
    public function actionContact() {
      $model = new ContactForm;
      if (isset($_POST['ContactForm'])) {
        $model->attributes = $_POST['ContactForm'];
        if ($model->validate()) {
          $headers = "From: {$model->email}\r\nReply-To: {$model->email}";
          mail(Yii::app()->params['adminEmail'], $model->subject, $model->body, $headers);
          Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
          $this->refresh();
        }
      }
      $this->render('contact', array('model' => $model));
    }

    /**
     * Displays the login page
     */
    public function actionLogin() {
      $model = new LoginForm;

      // if it is ajax validation request
      if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
        echo CActiveForm::validate($model);
        Yii::app()->end();
      }

      // collect user input data
      if (isset($_POST['LoginForm'])) {
        $model->attributes = $_POST['LoginForm'];
        // validate user input and redirect to the previous page if valid
        if ($model->validate() && $model->login())
          $this->redirect(Yii::app()->user->returnUrl);
      }
      // display the login form
      $this->render('login', array('model' => $model));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {

      Yii::app()->user->logout();
      $facebookLoggedIn = true;
      try {
        $userinfo = Yii::app()->facebook->getInfo();
      } catch (Exception $e) {
        $facebookLoggedIn = false;
      }

      Yii::app()->session->clear();
      Yii::app()->session->destroy();
      Yii::app()->facebook->destroySession();

      $this->redirect(Yii::app()->homeUrl);
    }

    public function actionProfile(){
      $data = User::model()->findBySql('SELECT * FROM user WHERE fb_id = "' . Yii::app()->facebook->getUser() . '"');
      if (isset($_POST['User'])) {
        $data->attributes = $_POST['User'];
        $data->created_time = date('Y-m-d H:i:s');
        if($data->save()){
          $this->redirect(array('site/home'));
        }else{
          $this->render('profile', array('data' => $data));
        }
      }else{
        $this->render('profile', array('data' => $data));
      }
    }

    public function actionHowto(){
      $this->render('howto');
    }

    public function actionArticle($text){
      $data = Article::model()->findBySql('SELECT * FROM article WHERE `text` = "' . $text . '"');
      $this->render('showarticle', array('data'=>$data));
    }

    public function actionSetpoint(){
       $data = User::model()->findBySql('SELECT * FROM user WHERE fb_id = "' . Yii::app()->facebook->getUser() . '"');
      if (count($data) != 0) {
        $data_article = 'at';
        $data_share = DataShare::model()->findBySql('SELECT * FROM data_share WHERE fb_id = "' . Yii::app()->facebook->getUser() . '" AND via = "' . $data_article . '" AND created_time="'.date('Y-m-d H:i:s')."'");
        if (count($data_share) == 0) {
          $model = new DataShare;
          $model->fb_id = Yii::app()->facebook->getUser();
          $model->via = $data_article;
          $model->created_time = date('Y-m-d H:i:s');
          if ($model->save()) {
            $data->point = $data->point + 20;
            $data->save();
          } else {
            echo 'save error';
          }
        } else {
          echo 'error';
        }
      } else {
        echo 'error';
      }
    }

    public function actionTutorial(){
      $this->render('tutorial');
    }
  }
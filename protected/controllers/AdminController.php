<?php

class AdminController extends Controller
{
  /**
   * Declares class-based actions.
   */
  public function actions()
  {

    return array(
      // captcha action renders the CAPTCHA image displayed on the contact page
      'captcha'=>array(
        'class'=>'CCaptchaAction',
        'backColor'=>0xFFFFFF,
      ),
      // page action renders "static" pages stored under 'protected/views/site/pages'
      // They can be accessed via: index.php?r=site/page&view=FileName
      'page'=>array(
        'class'=>'CViewAction',
      ),
    );
  }

  /**
   * This is the default 'index' action that is invoked
   * when an action is not explicitly requested by users.
   */
  public function actionIndex()
  {
    // renders the view file 'protected/views/site/index.php'
    // using the default layout 'protected/views/layouts/main.php'
    $this->layout = 'adminmain';
    if(!(Yii::app()->user->name == 'admin')){
      $this->redirect(array('admin/login'));
    }else{
      $this->redirect(array('admin/post'));
    }
  }

  /**
   * This is the action to handle external exceptions.
   */
  public function actionError()
  {
    if($error=Yii::app()->errorHandler->error)
    {
      if(Yii::app()->request->isAjaxRequest)
        echo $error['message'];
      else
        $this->render('error', $error);
    }
    $this->layout = 'adminmain';
  }

  /**
   * Displays the login page
   */
  public function actionLogin()
  {
    $model=new LoginForm;

    // if it is ajax validation request
    if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
    {
      echo CActiveForm::validate($model);
      Yii::app()->end();
    }

    // collect user input data
    if(isset($_POST['LoginForm']))
    {
      $model->attributes=$_POST['LoginForm'];
      // validate user input and redirect to the previous page if valid
      if($model->validate() && $model->login())
        $this->redirect(array('admin/index'));
    }
    // display the login form
    $this->layout = 'adminmain';
    $this->render('login',array('model'=>$model));
  }

  /**
   * Logs out the current user and redirect to homepage.
   */
  public function actionLogout()
  {
    Yii::app()->user->logout();
    $this->layout = 'adminmain';
    $this->redirect(array('admin/login'));
  }

  public function actionArticle(){

    $this->layout = 'adminmain';
    if(!(Yii::app()->user->name == 'admin')){
      $this->redirect(array('admin/login'));
    }else{
      $model = new Article;

      if (isset($_POST['Article'])) {
        $model->attributes = $_POST['Article'];
        $model->created_at = date('Y-m-d H:i:s');
        if($model->save()){
          $this->redirect(array('admin/post'));
        }else{
          $this->render('article',array('model'=>$model));
        }
        }else{
          $this->render('article',array('model'=>$model));
        }
      }
    }

  public function actionPost()
  {
    $this->layout = 'adminmain';
    if(!(Yii::app()->user->name == 'admin')){
      $this->redirect(array('admin/login'));
    }else{
      $data = Article::model()->findAll(array('order' => 'created_at DESC'));
      $this->render('post', array('data' => $data));
    }
  }

  public function actionEditarticle($id){
    $this->layout = 'adminmain';
    if(!(Yii::app()->user->name == 'admin')){
      $this->redirect(array('admin/login'));
    }else{
    $data = Article::model()->findBySql('SELECT * FROM article WHERE id = "' . $id . '"');

    if (isset($_POST['Article'])) {
      $data->attributes = $_POST['Article'];
      $data->created_at = date('Y-m-d H:i:s');
      if($data->save()){
        $this->redirect(array('admin/post'));
      }else{
        $this->render('editarticle', array('data' => $data));
      }
      }else{
        $this->render('editarticle', array('data' => $data));
      }
    }
  }

  public function actionDeletearticle($id){
    $this->layout = 'adminmain';
    if(!(Yii::app()->user->name == 'admin')){
      $this->redirect(array('admin/login'));
    }else{
    $data = Article::model()->findBySql('SELECT * FROM article WHERE id = "' . $id . '"');

      if($data->delete()){
        $this->redirect(array('admin/post'));
      }else{
        $this->redirect(array('admin/post'));
      }
    }
  }

  public function actionShowarticle($id){
    $this->layout = 'adminmain';
    if(!(Yii::app()->user->name == 'admin')){
      $this->redirect(array('admin/login'));
    }else{
    $data = Article::model()->findBySql('SELECT * FROM article WHERE id = "' . $id . '"');

      if($data->delete()){
        $this->redirect(array('admin/detailarticle'));
      }else{
        $this->redirect(array('admin/detailarticle'));
      }
    }
  }

  public function actionQuestions(){
    $this->layout = 'adminmain';
    $data = Question::model()->findAll(array('order' => 'schedule DESC'));
    $this->render('questions', array('data' => $data));
  }

  public function actionNewquest(){
    $this->layout = 'adminmain';
    $model = new Question;
    $choice = array();

    if (isset($_POST['Question'])) {
      $model->attributes = $_POST['Question'];
      for($i=0; $i<count($_POST['Question']['multiple_choice']); $i++){
        $ask = $i+1;
        $sem = $ask>=3 ? "" : ";";
        array_push($choice, $ask."==".$_POST['Question']['multiple_choice'][$i].$sem);
      }
      $model->multiple_choice = implode('',$choice);
      $model->schedule = date('Y-m-d H:i:s');
      if($model->save()){
        $this->redirect(array('admin/questions'));
      }else{
        $this->render('newquest',array('model'=>$model));
      }
    }else{
      $this->render('newquest',array('model'=>$model));
    }
  }

  public function actionEditquest($id){
    $this->layout = 'adminmain';
    $model = Question::model()->findBySql('SELECT * FROM question WHERE id = "' . $id . '"');
    $choice = array();

    if (isset($_POST['Question'])) {
      $model->attributes = $_POST['Question'];
      for($i=0; $i<count($_POST['Question']['multiple_choice']); $i++){
        $ask = $i+1;
        $sem = $ask>=3 ? "" : ";";
        array_push($choice, $ask."==".$_POST['Question']['multiple_choice'][$i].$sem);
      }
      $model->multiple_choice = implode('',$choice);
      $model->schedule = date('Y-m-d H:i:s');
      if($model->save()){
        $this->redirect(array('admin/questions'));
      }else{
        $this->render('editquest',array('model'=>$model));
      }
    }else{
      $this->render('editquest',array('model'=>$model));
    }
  }

  public function actionDeletequest($id){
    $this->layout = 'adminmain';
    if(!(Yii::app()->user->name == 'admin')){
      $this->redirect(array('admin/login'));
    }else{
    $data = Question::model()->findBySql('SELECT * FROM question WHERE id = "' . $id . '"');

      if($data->delete()){
        $this->redirect(array('admin/questions'));
      }else{
        $this->redirect(array('admin/questions'));
      }
    }
  }
}
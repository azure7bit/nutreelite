<?php

class Processing {
    
    static public function range($str, $fieldValue = NULL) {
	$rules = explode(';', $str);
	$array = array();
	for ($i = 0; $i < count($rules); $i++) {
	    $item = explode("==", $rules[$i]);
	    if (isset($item[0]))
		$array[$item[0]] = ((isset($item[1])) ? $item[1] : $item[0]);
	}
	if (isset($fieldValue))
	    if (isset($array[$fieldValue]))
		return $array[$fieldValue];
	    else
		return '';
	else
	    return $array;
    }

    public function widgetAttributes() {
	$data = array();
	$model = $this->getFields();

	foreach ($model as $field) {
	    if ($field->widget)
		$data[$field->varname] = $field->widget;
	}
	return $data;
    }
    
}

<div class="home">
  <div class="register">
    <div class="content bodyText">
      <?php
        $form = $this->beginWidget('CActiveForm', array(
          'id' => 'article-form',
          'enableAjaxValidation' => false,
        ));
      ?>
      <?php echo $form->labelEx($data, 'Nama Article'); ?><br />
      <?php echo $form->textField($data, 'text'); ?>
      <?php echo $form->error($data, 'text'); ?>
      <br /><br />
      <?php
        $this->widget('application.extensions.cleditor.ECLEditor', array(
          'model'=>$data,
          'attribute'=>'isi_article', //Model attribute name. Nome do atributo do modelo.
          'options'=>array(
              'width'=>'600',
              'height'=>250,
              'useCSS'=>true,
          ),
          'value'=>$data->isi_article, //If you want pass a value for the widget. I think you will. Se você precisar passar um valor para o gadget. Eu acho irá.
      ));
      ?>
      <br /><br />
      <?php echo CHtml::submitButton($data->isNewRecord ? 'Create' : 'Save'); ?>
      <?php $this->endWidget(); ?>
    </div>
  </div>
</div>
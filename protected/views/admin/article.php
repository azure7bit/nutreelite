<div class="home">
  <div class="register">
    <div class="content bodyText">
      <?php
        $form = $this->beginWidget('CActiveForm', array(
          'id' => 'user-form',
          'enableAjaxValidation' => false,
        ));
      ?>
      <?php echo $form->labelEx($model, 'Nama Article'); ?><br />
      <?php echo $form->textField($model, 'text'); ?>
      <?php echo $form->error($model, 'text'); ?>
      <br /><br />
      <?php
        $this->widget('application.extensions.cleditor.ECLEditor',
          array(
            'model'=>$model,
            'attribute'=>'isi_article',
            'options'=>array('width'=>'600', 'height'=>250, 'useCSS'=>true,),
            'value'=>$model->isi_article,
        ));
      ?>
      <br /><br />
      <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
      <?php $this->endWidget(); ?>
    </div>
  </div>
</div>
<div class="home">
  <div class="register">
    <div class="content bodyText">
      <?php
        $form = $this->beginWidget('CActiveForm', array(
          'id' => 'user-form',
          'enableAjaxValidation' => false,
          'enableClientValidation' => true,
          'clientOptions' => array(
              'validateOnSubmit' => true,
          ),
        ));
        $choices = explode(";", $model->multiple_choice);
      ?>
      <?php echo $form->labelEx($model, 'pertanyaan'); ?><br />
      <?php echo $form->textArea($model, 'text',array('size'=>90)); ?>
      <?php echo $form->error($model, 'text'); ?>
      <br /><br />
      <?php echo $form->labelEx($model, 'insert_choice'); ?><br />
      1.
      <?php echo $form->textField($model, 'multiple_choice[0]', array('value' => str_replace("1==", '', $choices[0]), 'required' => true)); ?>
      <br /><br />
      2.
      <?php echo $form->textField($model, 'multiple_choice[1]', array('value' => str_replace("2==", '', $choices[1]),'required' => true)); ?>
      <br /><br />
      3.
      <?php echo $form->textField($model, 'multiple_choice[2]', array('value' => str_replace("3==", '', $choices[2]),'required' => true)); ?>
      <br /><br />
      <?php echo $form->labelEx($model, 'the_aswer_is'); ?><br />
      <?php echo $form->textField($model, 'answer'); ?>
      <br /><br />
      <?php echo $form->labelEx($model, 'hint'); ?><br />
      <?php echo $form->textArea($model, 'hint'); ?>
      <br /><br />
      <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
      <?php $this->endWidget(); ?>
    </div>
  </div>
</div>
<div class="home">
  <div class="content bodyText">
    <p><?php echo CHtml::link('new question', array('admin/newquest')); ?></p>
  	<table>
      <thead>
      	<tr>
      		<th>No.</th>
      		<th>Isi pertanyaan</th>
      		<th>Schedule</th>
          <th>Action</th>
      	</tr>
      </thead>
      <tbody>
    		<?php for ($i = 0; $i < count($data); $i++) { ?>
        	<tr>
        		<td><?php echo ($i + 1); ?></td>
        		<td>
              <?php echo $data[$i]->text; ?>
        		</td>
        		<td>
    			     <?php echo $data[$i]->schedule; ?>
    		    </td>
            <td>
              <?php echo CHtml::link('edit', 'editquest?id=' . $data[$i]->id, array('admin/editquest')); ?>
              <?php echo CHtml::link('delete', 'deletequest?id=' . $data[$i]->id, array('admin/deletequest')); ?>
            </td>
      		</tr>
    		<?php } ?>
      </tbody>
    </table>
  </div>
</div>
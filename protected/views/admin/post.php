<div class="home">
  <div class="content bodyText">
    <p><?php echo CHtml::link('new article', array('admin/article')); ?></p>
  	<table>
      <thead>
      	<tr>
      		<th>No.</th>
      		<th>Nama Article</th>
      		<th>Tanggal Edit</th>
          <th>Action</th>
      	</tr>
      </thead>
      <tbody>
    		<?php for ($i = 0; $i < count($data); $i++) { ?>
        	<tr>
        		<td><?php echo ($i + 1); ?></td>
        		<td>
              <?php echo $data[$i]->text; ?>
        		</td>
        		<td>
    			     <?php echo $data[$i]->created_at; ?>
    		    </td>
            <td>
              <?php echo CHtml::link('edit', 'editarticle?id=' . $data[$i]->id, array('admin/editarticle')); ?>
              <?php echo CHtml::link('delete', 'deletearticle?id=' . $data[$i]->id, array('admin/deletearticle')); ?>
            </td>
      		</tr>
    		<?php } ?>
      </tbody>
    </table>
  </div>
</div>
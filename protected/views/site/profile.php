<div class="register">
  <div class="content bodyText">
    <?php
      $form = $this->beginWidget('CActiveForm', array(
        'id' => 'user-form',
        'enableAjaxValidation' => false,
      ));
    ?>
      <?php echo $form->labelEx($data, 'first_name'); ?><br />
      <?php echo $form->textField($data, 'first_name', array('maxlength' => 33)); ?>
      <?php echo $form->error($data, 'first_name'); ?>
      <br /><br />
      <?php echo $form->labelEx($data, 'last_name'); ?><br />
      <?php echo $form->textField($data, 'last_name', array('maxlength' => 33)); ?>
      <?php echo $form->error($data, 'last_name'); ?>
      <br /><br />
      <?php echo $form->labelEx($data, 'phone'); ?><br />
      <?php echo $form->textField($data, 'phone', array('maxlength' => 17)); ?>
      <?php echo $form->error($data, 'phone'); ?>
      <br /><br />
      <?php echo $form->labelEx($data, 'email'); ?><br />
      <?php echo $form->textField($data, 'email', array('maxlength' => 55)); ?>
      <?php echo $form->error($data, 'email'); ?>
      <br /><br />
      <?php echo CHtml::submitButton($data->isNewRecord ? 'Create' : 'Save', array('class'=>'save')); ?>
    <?php $this->endWidget(); ?>
  </div>
</div>
<?php $baseUrl = Yii::app()->request->baseUrl . "/library/images/"; ?>
<div class="getPoint">
  <div class="content bodyText hf">
    <div class="right">
      <div class="contentPart">
        <div class="innCont">
          <div class="headText">PERTANYAAN HARI INI</div>
          <?php if (count($question) != count($dataAns)) { ?>
            <?php
              $form = $this->beginWidget('CActiveForm', array(
                'id' => 'answer-form',
                'enableAjaxValidation' => false,
              ));
            ?>
              <div class="questCont">
                <?php echo CHtml::image($baseUrl . 'nutrilite_iconq.jpg', 'nutrilite image', array('class' => 'iconQ')); ?>
                <?php echo $question[count($dataAns)]->text; ?>
                <br>Hint: temukan jawabannya <a href="<?php echo $question[count($dataAns)]->hint; ?>" class='cg'>disini</a>
              </div>
              <div class="questCont">
                <?php echo CHtml::image($baseUrl . 'nutrilite_icona.jpg', 'nutrilite image', array('class' => 'iconQ')); ?>
                <?php echo $form->radioButtonList($model, 'answer', Processing::range($question[count($dataAns)]->multiple_choice)); ?>
              </div>
              <div class="btnCont">
                <input type="image" src="<?php echo $baseUrl . 'nutrilite_submit.png'; ?>" />
              </div>
            <?php $this->endWidget(); ?>
          <?php } else { ?>
            <div class="questCont">
              <?php echo CHtml::image($baseUrl . 'nutrilite_iconq.jpg', 'nutrilite image', array('class' => 'iconQ')); ?>
                Kamu sudah menjawab semua pertanyaan hari ini, Silahkan kembali lagi besok<br />
            </div>
            <div class="questCont">
              <?php echo CHtml::image($baseUrl . 'nutrilite_icona.jpg', 'nutrilite image', array('class' => 'iconQ')); ?> Cek
              <?php echo CHtml::link('Home', array('site/home'), array('class' => 'cg')); ?> untuk melihat score kamu saat ini.
            </div>
          <?php } ?>
        </div>
      </div>
      <?php echo CHtml::image($baseUrl . 'nutrilite_bgright.png', 'nutrilite image'); ?>
    </div>
    <div class="left">
      <div class="contentPart">
        <div class="innCont">
          <div class="headText">DAPATKAN EXTRA POINT</div>
          <div class="subBodyText">Share link sebanyak mungkin<br />untuk meraih extra 50 point:</div>
          <div class="yourLink">
            <?php echo Yii::app()->request->hostInfo . Yii::app()->request->baseURL . '/?d=' . Yii::app()->facebook->getUser(); ?>
          </div>
          <div class="shareCont">
            <div class="subBodyText">Share to:</div>
            <br />
            <div class="shareBtnCont">
              <?php echo CHtml::link(CHtml::image($baseUrl . 'nutrilite_iconfb.jpg', 'nutrilite image'), Yii::app()->request->hostInfo . Yii::app()->request->baseURL . '/?d=' . $data->fb_id, array('data-image' => '', 'data-desc' => 'Ayo ikutan dan pelihara pohon virtualmu.', 'class' => 'shareLink', 'data' => 'fb')); ?>
              <?php echo CHtml::link(CHtml::image($baseUrl . 'nutrilite_icontw.jpg', 'nutrilite image'), 'https://twitter.com/intent/tweet?url=' . Yii::app()->request->hostInfo . Yii::app()->request->baseURL . '/?d=' . $data->fb_id . '&amp;text=Ayo ikutan dan pelihara pohon virtualmu.&amp;via=sehatplus'); ?>
              <?php echo CHtml::link(CHtml::image($baseUrl . 'nutrilite_icongt.jpg', 'nutrilite image'), '#', array('onclick' => "popUp=window.open('https://plus.google.com/share?url=" . Yii::app()->request->hostInfo . Yii::app()->request->baseURL . '/?d=' . $data->fb_id . "', 'popupwindow', 'scrollbars=yes,width=800,height=400');popUp.focus();return false", 'class' => 'shareLink', 'data' => 'gt')); ?>
            </div>
          </div>
          <div class="statisticCont">
            <div class="subHeadText">Statistic Referral</div>
            <div class="note">(Persebaran link)</div>
            <table class="dataStatistic">
              <tr>
                <td>Jumlah Referral</td>
                <td><?php echo count($return); ?> Orang</td>
              </tr>
              <tr>
                <td>Point</td>
                <td><?php echo count($return) * 10; ?></td>
              </tr>
            </table>
          </div>
        </div>
      </div>
      <?php echo CHtml::image($baseUrl . 'nutrilite_bgleft.png', 'nutrilite image'); ?>
    </div>
  </div>
</div>
<script>
window.fbAsyncInit = function() {
  FB.init({
    appId: '<?php echo Yii::app()->facebook->appId; ?>', status: true, cookie: true, xfbml: true
  });
};

(function(d, debug) {
  var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
  if (d.getElementById(id)) { return;}
  js = d.createElement('script');
  js.id = id;
  js.async = true;
  js.src = "//connect.facebook.net/en_US/all" + (debug ? "/debug" : "") + ".js";
  ref.parentNode.insertBefore(js, ref);
}(document, /*debug*/ false));

function postToFeed(title, desc, url, image, type) {
  var obj = {method: 'feed', link: url, picture: image, name: title, description: desc};
  function callback(response) {
    if (response && response.post_id) {} else {}
  }
  FB.ui(obj, callback);
}

$('.shareLink').click(function() {
  elem = $(this);
  if (elem.attr('data') == 'fb') {
    postToFeed(elem.data('title'), elem.data('desc'), elem.prop('href'), elem.data('image'));
    return false;
  }
});

!function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
  if (!d.getElementById(id)) {
    js = d.createElement(s);
    js.id = id;
    js.src = p + '://platform.twitter.com/widgets.js';
    fjs.parentNode.insertBefore(js, fjs);
  }
}(document, 'script', 'twitter-wjs');

(function() {
  var po = document.createElement('script');
  po.type = 'text/javascript';
  po.async = true;
  po.src = 'https://apis.google.com/js/platform.js';
  var s = document.getElementsByTagName('script')[0];
  s.parentNode.insertBefore(po, s);
})();
</script>
<?php $baseUrl = Yii::app()->request->baseUrl . "/library/images/"; ?>
<div class="rules">
  <div class="content bodyText">
    <div class="headText">
      &ndash; CARA MAIN NUTRILITE VIRTUAL TREE &ndash;
    </div>
    <br />
    <div>
      <ul>
        <li>
          Cara mengikuti NUTREELITE &ndash; Nutrilite Virtual Tree:
          <ul>
            <li>Peserta masuk ke sehatplus.com/nutreelite.</li>
            <li>Peserta melakukan registrasi dengan menggunakan data diri yang benar dan lengkap.</li>
            <li>Masing-masing peserta akan mendapatkan sebuah ‘pohon’ virtual yang harus ditumbuhkan dengan menambah poin.</li>
            <li>Peserta peserta dengan poin tertinggi dan pohon yang paling subur akan menjadi pemenang.</li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</div>
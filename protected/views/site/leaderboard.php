<?php $baseUrl = Yii::app()->request->baseUrl . "/library/images/"; ?>

<div class="leaderBoard table-responsive">
  <div class="content bodyText">
    <table class="table table-striped" style="border-radius:10px;">
      <thead>
        <tr>
          <th class="col1" style="text-align:center">No.</th>
          <th class="col2" style="text-align:center">Name</th>
          <th class="col3" style="text-align:center">Point</th>
        </tr>
      </thead>
      <tbody>
        <?php for ($i = 0; $i < count($usersData); $i++) { ?>
          <tr>
            <td class="col1">
              <strong><?php echo ($i + 1); ?></strong>
            </td>
            <td class="col2">
              <strong>
                <?php echo CHtml::link($usersData[$i]->first_name, 'https://www.facebook.com/' . $usersData[$i]->fb_id, array('target' => '_blank')); ?>
              </strong>
            </td>
            <td class="col3">
              <strong><?php echo $usersData[$i]->point; ?></strong>
            </td>
          </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>
</div>

<div><?php $this->widget('CLinkPager', array('pages' => $pages)) ?></div>
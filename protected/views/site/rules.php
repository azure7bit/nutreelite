<?php $baseUrl = Yii::app()->request->baseUrl . "/library/images/"; ?>
<div class="rules">
  <div class="content bodyText">
    <div class="headText">
      &ndash; SYARAT DAN KETENTUAN NUTRILITE VIRTUAL TREE &ndash;
    </div>
    <br />
    <div class="syarat">
      <ul>
        <li>Program NUTREELITE &ndash; Nutrilite Virtual Tree diselenggarakan oleh Amway Indonesia.</li>
        <li>Program ini berlangsung dari 1 Juli &ndash; 15 September 2014.</li>
        <li>Program ini hanya berlaku di wilayah Indonesia.</li>
        <li>Cara mengikuti NUTREELITE &ndash; Nutrilite Virtual Tree:
          <ul>
            <li>Peserta masuk ke <a href="http://sehatplus.com/nutreelite">sehatplus.com/nutreelite</a>.</li>
            <li>Peserta melakukan registrasi dengan menggunakan data diri yang benar dan lengkap.</li>
            <li>Masing-masing peserta akan mendapatkan sebuah 'pohon' virtual yang harus ditumbuhkan dengan menambah poin.</li>
            <li>Peserta peserta dengan poin tertinggi dan pohon yang paling subur akan menjadi pemenang.</li>
          </ul>
        </li>
        <li>Tentang sistem poin. Poin akan didapatkan dari:
          <ul>
            <li>Registrasi &ndash; 50 poin</li>
            <li>Login &ndash; 10 Poin</li>
            <li>Menjawab daily trivia (benar) &ndash; 20 poin</li>
            <li>Share URL referral &ndash; 50 poin</li>
            <li>Share artikel &ndash; 20 poin</li>
          </ul>
        </li>
        <li>Tentang pemenang dan hadiah:
          <ul>
            <li>Lucky Winner</li>
            <li>Grand Prize</li>
            <li>
              Update poin tertinggi dan Lucky Winner bisa dilihat di Leaderboard sehatplus.com/nutreelite dan akan diupdate setiap hari.
            </li>
          </ul>
        </li>
        <li>
          Dengan ikut serta dalam kompetisi ini, maka peserta telah dianggap setuju dan mengijinkan Amway Indonesia untuk menyimpan dan
          menggunakan semua data/informasi yang peserta berikan untuk keperluan penjualan, pemasaran dan promosi.
        </li>
        <li>
          Peserta wajib memberikan semua informasi yang diminta dengan benar. Jika informasi yang diminta tidak benar atau tidak ada,
          maka peserta tidak dapat mengikuti kompetisi ini.
        </li>
        <li>
          Keputusan penyelenggara adalah final dan tidak dapat diganggu gugat.
        </li>
        <li>
          Jika karena alasan apapun pemberian hadiah tidak dapat berjalan sesuai rencana, penyelenggara memiliki hak untuk memodifikasi
          ketentuan pemberian hadiah yang senilai dengan hadiah yang telah dijanjikan.
        </li>
        <li>
          Amway Indonesia tidak bertanggung jawab atas kesalahan apapun yang berhubungan dengan promosi program ini, dimana kesalahan tersebut
          terjadi di luar pengawasan yang sewajarnya Amway Indonesia.
        </li>
        <li>
          Promo ini tidak dapat berlaku untuk karyawan, agency, biro iklan, dan pihak ketiga (termasuk anggota keluarga dari Amway Indonesia yang
          turut bekerjasama menyelenggarakan program ini.
        </li>
        <li>
          Program ini tidak dipungut biaya. Pajak hadiah ditanggung pihak Amway Indonesia. Hati-hati terhadap penipuan.
        </li>
        <li>
          Amway Indonesia berhak untuk memodifikasi syarat dan ketentuan serta peraturan-peraturan dari program ini tanpa pemberitahuan terlebih dahulu.
        </li>
        <li>
          Informasi terkini mengenai kompetisi ini bisa didapatkan dengan cara bergabung di akun twitter (@sehatplus) serta Facebook fanpage (Sehat Plus & Amway Indonesia).
        </li>
      </ul>
    </div>
  </div>
</div>
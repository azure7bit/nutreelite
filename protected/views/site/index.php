<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="en" />
    <link rel="icon" type="image/ico" href="<?php echo Yii::app()->request->baseUrl; ?>/library/favicon.ico" />

    <!--[if lt IE 8]>
      <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/library/library/css/ie.css" media="screen, projection" />
    <![endif]-->

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/library/css/main.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/library/css/custom-main.css" />

    <?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/library/js/jquery-1.8.2.js'); ?>
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl.'/library/js/index.js';?>" defer="defer"></script>

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
  </head>
  <body>
    <?php $baseUrl = Yii::app()->request->baseUrl . "/library/images/"; ?>
    <div class="bgCont">
      <?php echo CHtml::image($baseUrl . 'nutrilite_bg.jpg', 'nutrilite image', array('class' => 'picFXY')); ?>
      <?php echo CHtml::image($baseUrl . 'nutrilite_tree.png', 'nutrilite image', array('id' => 'picTree')); ?>
      <?php echo CHtml::image($baseUrl . 'nutrilite_leaftr.png', 'nutrilite image', array('class' => 'leaf leaft leafr')); ?>
      <?php echo CHtml::image($baseUrl . 'nutrilite_leaftl.png', 'nutrilite image', array('class' => 'leaf leaft leafl')); ?>
      <?php echo CHtml::image($baseUrl . 'nutrilite_leafbr.png', 'nutrilite image', array('class' => 'leaf leafb leafr')); ?>
      <?php echo CHtml::image($baseUrl . 'nutrilite_leafbl.png', 'nutrilite image', array('class' => 'leaf leafb leafl')); ?>
    </div>
    <div class="mainMenuCont" style="background: none; height: 15px;"></div>
    <div class="logoCont">
      <?php echo CHtml::image($baseUrl . 'nutrilite_logo.png', 'nutrilite image', array('class' => 'piclogo')); ?>
    </div>
    <div id="page">
      <div class="container">
        <div class="index">
          <?php echo CHtml::image($baseUrl . 'nutrilite_cele.png', 'nutrilite image'); ?>
          <div class="highlighttext">Tumbuhkan Pohon, Kumpulkan Poin, Raih Hadiahnya</div>
          <div class="btnCont hf">
            <div class="left"><?php echo CHtml::link('Login', array('site/fblogin'), array('class' => 'btnLink shake')); ?></div>
            <div class="right"><?php echo CHtml::link('Sign Up', array('site/fblogin'), array('class' => 'btnLink shake')); ?></div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
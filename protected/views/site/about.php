<?php $baseUrl = Yii::app()->request->baseUrl . "/library/images/"; ?>
<div class="about">
  <div class="content bodyText">
    <center><div class="headText">&ndash; TENTANG NUTRILITE &ndash;</div></center>
    <br /><br />
    <div class="col-lg-12">
    <div class="data-article" style="position:relative;">
      <?php for ($i = 0; $i < count($data); $i++) { ?>
        <div class="col-lg-4 thelist">
          <h4 align="left">
            <?php echo CHtml::link($data[$i]->text, 'article?text=' . $data[$i]->text, array('site/showarticle', 'style'=>'text-transform:uppercase;font-weight:bold;')); ?>
          </h4>
          <p><?php echo "Post on ". $data[$i]->created_at; ?></p>
          <p><?php echo truncate($data[$i]->isi_article);?></p>
          <p><?php echo CHtml::link('Read More', 'article?text=' . $data[$i]->text, array('site/showarticle', 'style'=>'float:right;', 'class'=>'btn btn-sm btn-success')); ?></p>
          <hr/>
        </div>
      <?php } ; ?>
    </div>
    </div>
    <div class="clearfix"></div>
  </div>
</div>
<div><?php $this->widget('CLinkPager', array('pages' => $pages)) ?></div>
<?php $baseUrl = Yii::app()->request->baseUrl . "/library/images/"; ?>

<?php
  if ($data->tree_cond >= 5) {
    $cond = 'Sangat Sehat';
    $pathimage = $baseUrl."sangat_sehat/";
  } else if ($data->tree_cond == 4 ) {
    $cond = 'Sehat';
    $pathimage = $baseUrl."sehat/";
  } else if ($data->tree_cond == 3) {
    $cond = 'Kurang Sehat';
    $pathimage = $baseUrl."kurang_sehat/";
  } else {
    $cond = 'Tidak Sehat';
    $pathimage = $baseUrl."tidak_sehat/";
  }

  $point = $data->point;
  if($point >= 2000) {
    $pohon = $pathimage."10.gif";
  }
  else if ($point >= 1800 && $point < 2000){
   $pohon = $pathimage."9.gif";
  }
  else if ($point >= 1600 && $point < 1800) {
    $pohon = $pathimage."8.gif";
  }
  else if ($point >= 1400 && $point < 1600){
    $pohon = $pathimage."7.gif";
  }
  else if ($point >= 1200 && $point < 1400){
    $pohon = $pathimage."6.gif";
  }
  else if ($point >= 1000 && $point < 1200){
    $pohon = $pathimage."5.gif";
  }
  else if ($point >= 800 && $point < 1000){
    $pohon = $pathimage."4.gif";
  }
  else if ($point >= 600 && $point < 800){
    $pohon = $pathimage."3.gif";
  }
  else if ($point >= 400 && $point < 600){
    $pohon = $pathimage."2.gif";
  }
  else if ($point < 400){
    $pohon = $pathimage."1.gif";
  }
?>

<?php $req = Yii::app()->urlManager->parseUrl(Yii::app()->request);
  if($req=='site/home'){
    if (count($data) == 0) {
      echo CHtml::image($baseUrl . 'nutrilite_tree.png', 'nutrilite image', array('id' => 'picTree'));
    }else{
  ?>
  <div id="myGallery" style="float: left;top:0px;width: 400px;">
    <br /><br />
    <?php echo CHtml::image($pohon,'nutrilite image', array('class'=>'active img-responsive'));?>
  </div>
<?php } } ?>
<div class="home">
  <div class="content bodyText" style="padding:45px;">
    <div class="headText">&ndash; KONDISI POHON &ndash;</div>
    <br />
    <div class="treeData"><?php echo $cond; ?></div>
    <br /><br />
    <div class="headText">&ndash; POINT ANDA &ndash;
    </div>
    <br />
    <div class="treeData"><?php echo $data->point; ?></div>
    <br /><br />
    <?php echo CHtml::link('
      <div class="link">SHARE</div>',
      Yii::app()->request->hostInfo . Yii::app()->request->baseURL . '/' . $data->fb_id . '', array('data-image' => '',
      'data-desc' => $data->first_name, 'class' => 'fbShare', 'data' => 'fb', "title"=>"Share dan dapatkan poin ekstra!")); ?>
      <br />
    <?php echo CHtml::link('<div class="link">GET POINT</div>', array('site/getpoint'), array("title"=>"Tambahkan nutrisi untuk pohon virtual Anda dengan daily trivia disini")); ?>
  </div>
</div>

<script>
  function share(type) {
    var p = type;
    if (p != '') {
        $.ajax({
      type: 'POST',
      data: {p: p},
      url: 'share',
      beforeSend: function() {
          // todo
      },
      success: function(html) {
          if (html != 'error') {
        location.reload();
          } else {
        alert('error');
          }
      },
      error: function(xhr, ajaxOptions, thrownError) {
          alert(xhr.status);
          alert(xhr.responseText);
          alert(thrownError);
      }
        });
    } else {
        alert('please input date field');
    }
  }

  window.fbAsyncInit = function() {
    FB.init({
      appId: '<?php echo Yii::app()->facebook->appId; ?>', status: true, cookie: true, xfbml: true});
    };
    (function(d, debug) {
      var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
    if (d.getElementById(id)) {
      return;
    }
    js = d.createElement('script');
    js.id = id;
    js.async = true;
    js.src = "//connect.facebook.net/en_US/all" + (debug ? "/debug" : "") + ".js";
    ref.parentNode.insertBefore(js, ref);
  }(document, /*debug*/ false));
  function postToFeed(title, desc, url, image, type) {
    var obj = {method: 'feed', link: url, picture: image, name: title, description: desc};
    function callback(response) {
      if (response && response.post_id) {
        share(type);
        //    alert('Post was published.');
        //    window.location = 'http://localhost/scoopyquestion/site/wanttobuy';
      } else {
        //    alert('Post was not published.');
      }
    }
    FB.ui(obj, callback);
  }
  $('.fbShare').click(function() {
    elem = $(this);
    postToFeed(elem.data('title'), elem.data('desc'), elem.prop('href'), elem.data('image'), elem.attr('data'));
    return false;
  });
</script>

<?php $baseUrl = Yii::app()->request->baseUrl . "/library/images/"; ?>
<div class="about">
  <div class="content bodyText">
    <div class="headText" align="center">
      &ndash; TENTANG NUTRILITE &ndash;
    </div><br />
    <div class="data-article" style="position:relative;">
      <div align="center"><h4 style="font-weight:bold;"><?php echo $data->text; ?></h4></div>
    </div><br />
    <p><?php echo $data->isi_article;?></p>
    <hr/>

    <div class="social-article" style="text-align:left;">
      <div class="shareBtnCont">
        <?php echo CHtml::link(CHtml::image($baseUrl . 'nutrilite_iconfb.jpg', 'nutrilite image'), Yii::app()->request->hostInfo . Yii::app()->request->baseURL . '/?text=' . $data->text, array('data-image' => '', 'data-desc' => $data->isi_article, 'class' => 'shareLink', 'data' => 'fb', 'id'=>'fbart')); ?>
        <?php echo CHtml::link(CHtml::image($baseUrl . 'nutrilite_icontw.jpg', 'nutrilite image'), 'https://twitter.com/intent/tweet?url=' . Yii::app()->request->hostInfo . Yii::app()->request->baseURL . '/?text=' . $data->text . '&amp;text='.$data->text.'.&amp;via=sehatplus'); ?>
        <?php echo CHtml::link(CHtml::image($baseUrl . 'nutrilite_icongt.jpg', 'nutrilite image'), '#', array('onclick' => "popUp=window.open('https://plus.google.com/share?url=" . Yii::app()->request->hostInfo . Yii::app()->request->baseURL . '/?text=' . $data->text . "', 'popupwindow', 'scrollbars=yes,width=800,height=400');popUp.focus();return false", 'class' => 'shareLink', 'data' => 'gt')); ?>
        <input type="hidden" id="hidden_article" >
      </div>
    </div>
  </div>
</div>

<script>
$(document).ready(function(){
  var img_src= $('.thumb').find('img').attr('src');
  $('a#fbart').attr('data-image',img_src);
  $('.social-provider-icon').each(function(){
    $(this).click(function(){
      $.ajax({
        headers: {
          'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        url: "<?php echo Yii::app()->request->baseUrl . '/site/setpoint' ?>",
        type: 'GET',
        dataType: 'json',
        success: function(data){window.location.reload();},
        error: function(data){}
      });
    });
  });
});
</script>

<script>
window.fbAsyncInit = function() {
  FB.init({
    appId: '<?php echo Yii::app()->facebook->appId; ?>', status: true, cookie: true, xfbml: true
  });
};

(function(d, debug) {
  var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
  if (d.getElementById(id)) { return;}
  js = d.createElement('script');
  js.id = id;
  js.async = true;
  js.src = "//connect.facebook.net/en_US/all" + (debug ? "/debug" : "") + ".js";
  ref.parentNode.insertBefore(js, ref);
}(document, /*debug*/ false));

function postToFeed(title, desc, url, image, type) {
  var obj = {method: 'feed', link: url, picture: image, name: title, description: desc};
  function callback(response) {
    if (response && response.post_id) {} else {}
  }
  FB.ui(obj, callback);
}

$('.shareLink').click(function() {
  elem = $(this);
  if (elem.attr('data') == 'fb') {
    postToFeed(elem.data('title'), elem.data('desc'), elem.prop('href'), elem.data('image'));
    return false;
  }
});

!function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
  if (!d.getElementById(id)) {
    js = d.createElement(s);
    js.id = id;
    js.src = p + '://platform.twitter.com/widgets.js';
    fjs.parentNode.insertBefore(js, fjs);
  }
}(document, 'script', 'twitter-wjs');

(function() {
  var po = document.createElement('script');
  po.type = 'text/javascript';
  po.async = true;
  po.src = 'https://apis.google.com/js/platform.js';
  var s = document.getElementsByTagName('script')[0];
  s.parentNode.insertBefore(po, s);
})();
</script>
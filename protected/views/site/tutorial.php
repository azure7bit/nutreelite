<div class="rules syarat">
  <div class="content bodyText">
  <div class="headText">
    &ndash; SELAMAT DATANG DI NUTREELITE VIRTUAL TREE&ndash;
  </div>
  <br />
  <div>
    <ul>
      <li>Program NUTREELITE &ndash; Nutrilite Virtual Tree diselenggarakan oleh Amway Indonesia.</li>
      <li>Program ini berlangsung dari XXX &ndash; XXX 2014.</li>
      <li>Program ini hanya berlaku di wilayah Indonesia.</li>
      <li>Cara mengikuti NUTREELITE &ndash; Nutrilite Virtual Tree:
        <ul>
          <li>Peserta masuk ke sehatplus.com/nutreelite.</li>
          <li>Peserta melakukan registrasi dengan menggunakan data diri yang benar dan lengkap.</li>
          <li>Masing-masing peserta akan mendapatkan sebuah ‘pohon’ virtual yang harus ditumbuhkan dengan menambah poin.</li>
          <li>Peserta peserta dengan poin tertinggi dan pohon yang paling subur akan menjadi pemenang.</li>
        </ul>
      </li>
      <li>Tentang sistem poin. Poin akan didapatkan dari:
        <ul>
          <li>Registrasi &ndash; 50 poin</li>
          <li>Login &ndash; 10 Poin</li>
          <li>Menjawab daily trivia (benar) &ndash; 20 poin</li>
          <li>Share URL referral &ndash; 25 poin</li>
          <li>Share artikel &ndash; 20 poin</li>
        </ul>
      </li>
    </ul>
  </div>
  </div>
</div>
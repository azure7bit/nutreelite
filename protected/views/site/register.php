<?php $baseUrl = Yii::app()->request->baseUrl . "/library/images/"; ?>

<div class="register">
  <div class="content bodyText">
    <?php
      $form = $this->beginWidget('CActiveForm', array(
        'id' => 'user-form',
        'enableAjaxValidation' => false,
      ));
    ?>

      <?php echo $form->labelEx($model, 'first_name'); ?><br />
      <?php echo $form->textField($model, 'first_name', array('maxlength' => 33, 'class' => 'form-control')); ?>
      <?php echo $form->error($model, 'first_name'); ?>

      <br /><br />

      <?php echo $form->labelEx($model, 'last_name'); ?><br />
      <?php echo $form->textField($model, 'last_name', array('maxlength' => 33, 'class' => 'form-control')); ?>
      <?php echo $form->error($model, 'last_name'); ?>

      <br /><br />

      <?php echo $form->labelEx($model, 'phone'); ?><br />
      <?php echo $form->textField($model, 'phone', array('maxlength' => 17, 'class' => 'form-control')); ?>
      <?php echo $form->error($model, 'phone'); ?>
      <br /><br />

      <?php echo $form->labelEx($model, 'email'); ?><br />
      <?php echo $form->textField($model, 'email', array('maxlength' => 55, 'class' => 'form-control')); ?>
      <?php echo $form->error($model, 'email'); ?>

      <br /><br />

      <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn-sm btn-success')); ?>
    <?php $this->endWidget(); ?>
  </div>
</div>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="en" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="icon" type="image/ico" href="<?php echo Yii::app()->request->baseUrl; ?>/library/favicon.ico" />

    <!--[if lt IE 8]>
      <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/library/library/css/ie.css" media="screen, projection" />
    <![endif]-->

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/library/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/library/css/main.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/library/css/custom-main.css" />
    <link href='http://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Abel' rel='stylesheet' type='text/css'>

    <?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/library/js/common.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/library/js/jquery-1.8.2.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/library/js/bootstrap.min.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/library/js/pictree.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/library/js/about.js'); ?>

    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl.'/library/js/showarticle.js';?>" defer="defer"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl.'/library/js/index.js';?>" defer="defer"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl.'/library/js/home.js';?>" defer="defer"></script>

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
  </head>
  <body>
    <?php $baseUrl = Yii::app()->request->baseUrl . "/library/images/"; ?>
    <div class="bgCont">
	    <?php echo CHtml::image($baseUrl . 'nutrilite_bg.jpg', 'nutrilite image', array('class' => 'picFXY')); ?>
	    <?php
        $data = User::model()->findBySql('SELECT * FROM user WHERE fb_id = "' . Yii::app()->facebook->getUser() . '"');
        $req = Yii::app()->urlManager->parseUrl(Yii::app()->request);
        if($req=='site/home'){
          if (count($data) == 0) {
            echo CHtml::image($baseUrl . 'nutrilite_tree.png', 'nutrilite image', array('id' => 'picTree'));
          }
        }else{
          echo CHtml::image($baseUrl . 'nutrilite_tree.png', 'nutrilite image', array('id' => 'picTree'));
        }
      ?>
	    <?php echo CHtml::image($baseUrl . 'nutrilite_leaftr.png', 'nutrilite image', array('class' => 'leaf leaft leafr')); ?>
	    <?php echo CHtml::image($baseUrl . 'nutrilite_leaftl.png', 'nutrilite image', array('class' => 'leaf leaft leafl')); ?>
	    <?php echo CHtml::image($baseUrl . 'nutrilite_leafbr.png', 'nutrilite image', array('class' => 'leaf leafb leafr')); ?>
	    <?php echo CHtml::image($baseUrl . 'nutrilite_leafbl.png', 'nutrilite image', array('class' => 'leaf leafb leafl')); ?>
	  </div>
    <?php if (count($data) != 0) { ?>
      <div class="profile-link">
        <ul class="menuCont hf">
          <li><strong><?php echo "Hai ". $data->first_name . " " . $data->last_name;?></strong></li>
          <li><?php echo CHtml::link('EDIT PROFILE', array('site/profile')); ?></li>
        </ul>
      </div>
    <?php } ?>
    <div id="main-page">
      <div class="logobrand"><img class="eightith shake" src="<?php echo $baseUrl.'nutrilite_80th.png'; ?>" alt="nutrilite image"></div>
    	<div class="mainMenuCont">
        <div class="navbar navbar-default navbar-static-top" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
            </div>
            <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <li><?php echo CHtml::link('HOME', array('site/home')); ?></li>
                <li><?php echo CHtml::link('LEADERBOARD', array('site/leaderboard'), array("title"=>"Lihat siapa yang meraih poin tertinggi")); ?></li>
                <li><?php echo CHtml::link('GET POINT', array('site/getpoint'), array("title"=>"Tambah terus poin Anda, selesaikan tantangannya setiap hari")); ?></li>
              </ul>
              <ul class="nav navbar-nav navbar-right">
                <li><?php echo CHtml::link('SYARAT &amp; KETENTUAN', array('site/rules'), array("title"=>"Info lengkap tentang Nutrilite Virtual Tree")); ?></li>
                <li><?php echo CHtml::link('TENTANG NUTRILITE', array('site/aboutus'), array("title"=>"Ingin poin ekstra? Share artikel seputar Nutrilite di sini")); ?></li>
                <li class="logout"><?php echo CHtml::link('LOGOUT', array('site/logout')); ?></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
	  <div class="logoCont"><?php echo CHtml::image($baseUrl . 'nutrilite_logo.png', 'nutrilite image', array('class' => 'piclogo')); ?></div>
    <div id="page"><?php echo $content; ?></div>
  </body>
</html>
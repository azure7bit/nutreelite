$(function() {
  var pull = $('#pull');
    menu = $('nav ul');
    menuHeight = menu.height();
  $(pull).on('click', function(e) {
    e.preventDefault();
    menu.slideToggle();
  });
});

$(window).load(function() {
  widowHeight = $(window).height();
  $('#picTree').animate({top: ((widowHeight - $('#picTree').height()) / 2) + 70}, 0);
});

$(window).resize(function() {
  widowHeight = $(window).height();
  $('#picTree').animate({top: ((widowHeight - $('#picTree').height()) / 2) + 70}, 0);
  var w = $(window).width();
  if(w > 600 && menu.is(':hidden')) {menu.removeAttr('style');}
});
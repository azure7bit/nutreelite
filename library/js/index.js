function getImgSize(imgSrc) {
  var newImg = new Image();

  newImg.onload = function() {
    var height = newImg.height;
    var width = newImg.width;
  }

  newImg.src = imgSrc;
}

$(window).load(function() {
  widowHeight = $(window).height();
  $('#picTree').animate({top: ((widowHeight - $('#picTree').height()) / 2) + 70}, 0);
});

$(window).resize(function() {
  widowHeight = $(window).height();
  $('#picTree').animate({top: ((widowHeight - $('#picTree').height()) / 2) + 70}, 0);
});

$(document).ready(function() {
  pathImage = $("#picTree").attr('src');
  getImgSize(pathImage);
});